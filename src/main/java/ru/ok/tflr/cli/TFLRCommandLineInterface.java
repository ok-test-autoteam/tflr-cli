package ru.ok.tflr.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public final class TFLRCommandLineInterface {

    private TFLRCommandLineInterface() {
    }

    public static TFLRParameters extractParameters(TFLRParameters arguments, String[] args) {
        JCommander jcommander = new JCommander(arguments);

        try {
            jcommander.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
        }
        return arguments;
    }
}
