package ru.ok.tflr.cli;

import com.beust.jcommander.Parameter;

public class TFLRParameters {

    @Parameter(names = {"-dir", "-d"}, description = "Root directories for sources or files, comma-separated.", required = true)
    private String sourceDirs;

    @Parameter(names = {"-ruleset", "-r"}, description = "Path to YAML ruleset.", required = true)
    private String rulesConfigYaml;

    @Parameter(names = {"-minimumpriority", "-min"}, description = "Minimum priority of rules usage.")
    private int minimumPriority = 5;

    @Parameter(names = {"-enableoutput", "-out"}, description = "Enable rule violations output to console.")
    private boolean outputEnable = false;

    public String getSourceDirs() {
        return sourceDirs;
    }

    public String getRulesConfigYaml() {
        return rulesConfigYaml;
    }

    public int getMinimumPriority() {
        return minimumPriority;
    }

    public boolean isOutputEnable() {
        return outputEnable;
    }
}
