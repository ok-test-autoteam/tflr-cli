package ru.ok.tflr;

import ru.ok.tflr.cli.TFLRCommandLineInterface;
import ru.ok.tflr.cli.TFLRParameters;

import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.TFLRRuleViolation;

import java.util.*;

public class CliRunner {

    public static final int PRIORITY_FOR_ERRORS = 2;
    private static final String DELIMITER = ",";

    /**
     * Parses the command line arguments and XML config file and executes CliRunner.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Starting...");

        final TFLRParameters params = TFLRCommandLineInterface.extractParameters(new TFLRParameters(), args);

        List<String> listSourceFiles = Arrays.asList(params.getSourceDirs().split(DELIMITER));

        long start = System.currentTimeMillis();
        TFLRResult result = TFLR.runTFLR(listSourceFiles, params.getRulesConfigYaml(), TFLRRulePriority.valueOf(params.getMinimumPriority()));
        long finish = System.currentTimeMillis();

        if (params.isOutputEnable()) printViolations(result);

        System.out.println("Done (" + (finish - start) + " ms). Process finished with exit code " + result.getStatus().getStatusId()
                + " (" + result.getStatus().name() + ").");
        System.exit(result.getStatus().getStatusId());
    }

    private static void printViolations(TFLRResult result) {
        for (Map.Entry<String, TFLRReport> currentReport : result.getReports().entrySet()) {
            String filename = currentReport.getKey();
            TFLRReport report = currentReport.getValue();
            System.out.println("Violations in file - " + filename);
            Iterator<TFLRRuleViolation> iterator = report.iterator();
            int counter = 1;
            while (iterator.hasNext()) {
                TFLRRuleViolation violation = iterator.next();
                System.out.format("  [%2d] " + violation.getPackageName() + "." + violation.getClassName() + ":%3d - " + violation.getMessage() + "\n",
                        counter++, violation.getBeginLine());
            }

        }
    }
}